INSERT INTO flower (id, title, cost)
VALUES (1, 'Роза', 100);
INSERT INTO flower (id, title, cost)
VALUES (2, 'Лилия', 50);
INSERT INTO flower (id, title, cost)
VALUES (3, 'Ромашка', 25);

INSERT INTO buyer (id, name, surname, telephone_number, address)
VALUES (1, 'Alexey', 'Ivanov', '89502345463', 'Lenina st., 31');
INSERT INTO buyer (id, name, surname, telephone_number, address)
VALUES (2, 'Ivan', 'Petrov', '89535622347', 'Tsialkovskogo st., 10');
INSERT INTO buyer (id, name, surname, telephone_number, address)
VALUES (3, 'Sergey', 'Smirnov', '89234379064', 'Novaya st., 4');

INSERT INTO ordering (id, date, amount, flower_id, buyer_id)
VALUES (1, now(), 11, 1, 1);
INSERT INTO ordering (id, date, amount, flower_id, buyer_id)
VALUES (2, now(), 13, 1, 2);
INSERT INTO ordering (id, date, amount, flower_id, buyer_id)
VALUES (3, now(), 7, 2, 2);
INSERT INTO ordering (id, date, amount, flower_id, buyer_id)
VALUES (4, now(), 21, 2, 3);
INSERT INTO ordering (id, date, amount, flower_id, buyer_id)
VALUES (5, now(), 33, 3, 1);
INSERT INTO ordering (id, date, amount, flower_id, buyer_id)
VALUES (6, now() - interval '1 year', 33, 3, 1);
INSERT INTO ordering (id, date, amount, flower_id, buyer_id)
VALUES (7, now() - interval '1 day', 55, 2, 1);
