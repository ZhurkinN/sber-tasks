CREATE TABLE flower (
    id BIGINT PRIMARY KEY,
    title varchar(30) NOT NULL,
    cost BIGINT NOT NULL
);

CREATE TABLE buyer (
    id BIGINT PRIMARY KEY,
    name varchar(30) NOT NULL,
    surname varchar(40),
    telephone_number varchar(20) NOT NULL,
    address varchar(50) NOT NULL
);

CREATE TABLE ordering (
    id BIGINT PRIMARY KEY,
    date DATE,
    amount INTEGER NOT NULL,
    flower_id BIGINT NOT NULL,
    buyer_id BIGINT NOT NULL,
    CONSTRAINT value_check CHECK (amount > 0 AND amount < 1001),
    CONSTRAINT buyer_fk FOREIGN KEY (buyer_id)
        REFERENCES buyer (id)
        ON DELETE CASCADE,
    CONSTRAINT flower_fk FOREIGN KEY (flower_id)
        REFERENCES flower (id)
        ON DELETE CASCADE
)