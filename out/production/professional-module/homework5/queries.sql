SELECT b.name, b.telephone_number, f.title, ordering.amount, ordering.date
FROM ordering
INNER JOIN buyer b ON b.id = ordering.buyer_id
INNER JOIN flower f on f.id = ordering.flower_id
WHERE ordering.id = 1;

SELECT f.title, ordering.amount, ordering.date
FROM ordering
INNER JOIN buyer b on b.id = ordering.buyer_id
INNER JOIN flower f on f.id = ordering.flower_id
WHERE date >= (now() - interval '1 month') AND date < now() AND b.id = 1;

SELECT f.title, amount AS Maximum_Amount
FROM ordering
INNER JOIN flower f on f.id = ordering.flower_id
WHERE amount = (SELECT MAX(amount) FROM ordering);

SELECT SUM(f.cost * ordering.amount)
FROM ordering
INNER JOIN flower f on f.id = ordering.flower_id
