package homework3.part1.task7;

public class TriangleTester {
    public static void main(String[] args) {
        showResult(5, 2, 7);
        showResult(12, 12, 19);
        showResult(6, 8, 1);
        showResult(9, 1, 2);
        showResult(7, 14, 10);
    }

    private static void showResult(double firstSide, double secondSide, double thirdSide) {
        System.out.println("Со сторонами: " + firstSide + ", " + secondSide + ", " + thirdSide + " - " + TriangleChecker.checkTriangle(firstSide, secondSide, thirdSide));
    }
}
