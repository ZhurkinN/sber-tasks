package homework3.part1.task7;

public class TriangleChecker {

    private TriangleChecker() {
        super();
    }

    public static boolean checkTriangle(double firstSide, double secondSide, double thirdSide) {
        return (firstSide + secondSide > thirdSide) && (firstSide + thirdSide > secondSide) && (secondSide + thirdSide) > firstSide;
    }
}
