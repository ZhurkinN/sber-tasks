package homework3.part1.task3;

import homework3.part1.task2.Student;

import java.util.Arrays;

public class StudentService {

    private StudentService() {
        super();
    }

    public static Student getBestStudent(Student[] students) {
        double max = students[0].getAverageGrade();
        int maxIndex = 0;
        for (int i = 1; i < students.length; i++) {
            double averageGrade = students[i].getAverageGrade();
            if (averageGrade > max) {
                max = averageGrade;
                maxIndex = i;
            }
        }
        return students[maxIndex];
    }

    public static void sortBySurname(Student[] students) {
        Arrays.sort(students);
    }
}
