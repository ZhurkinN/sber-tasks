package homework3.part1.task6;

public class AmazingString {

    private char[] string;

    private AmazingString() {
        super();
    }

    public AmazingString(char[] string) {
        super();
        this.string = string;
    }

    public AmazingString(String string) {
        this.string = string.toCharArray();
    }

    public char getCharacterByIndex(int index) {
        return this.string[index - 1];
    }

    public int getStringSize() {
        return this.string.length;
    }

    public void printString() {
        System.out.println();
        for (char character : string) {
            System.out.print(character);
        }
    }

    public boolean containsSubstring(char[] substring) {
        int index = 0;
        boolean contains = false;
        for (int i = 0; i < this.string.length; i++) {
            if (this.string[i] == substring[index]) {
                index++;
                for (int j = i + 1; j < i + substring.length; j++) {
                    contains = true;
                    if (this.string[j] == substring[index]) {
                        index++;
                    } else {
                        index = 0;
                        contains = false;
                        break;
                    }
                }
            }
            if (contains) {
                break;
            }
        }
        return contains;
    }

    public boolean containsSubstring(String substring) {
        AmazingString amazingString = new AmazingString(substring);
        return containsSubstring(amazingString.getString());
    }

    public void stripLeft() {
        int lastIndex = 0;
        while (this.string[lastIndex] == ' ' || this.string[lastIndex] == '\n' || this.string[lastIndex] == '\t') {
            lastIndex++;
        }
        if (lastIndex > 0) {
            char[] newString = new char[this.string.length - lastIndex];
            for (int i = 0; i < newString.length; i++) {
                newString[i] = this.string[lastIndex];
                lastIndex++;
            }
            this.string = newString;
        }
    }

    public void reverseString() {
        for (int i = 0; i < this.string.length / 2; i++) {
            char temp = this.string[i];
            this.string[i] =  this.string[this.string.length - 1 - i];
            this.string[this.string.length - 1 - i] = temp;
        }
    }

    public char[] getString() {
        return string;
    }

    public void setString(char[] string) {
        this.string = string;
    }
}
