package homework3.part1.task5;

public class WeekRunner {
    public static void main(String[] args) {
        DayOfWeek monday = new DayOfWeek((byte) 1, "Monday");
        DayOfWeek tuesday = new DayOfWeek((byte) 2, "Tuesday");
        DayOfWeek wednesday = new DayOfWeek((byte) 3, "Wednesday");
        DayOfWeek thursday = new DayOfWeek((byte) 4, "Thursday");
        DayOfWeek friday = new DayOfWeek((byte) 5, "Friday");
        DayOfWeek saturday = new DayOfWeek((byte) 6, "Saturday");
        DayOfWeek sunday = new DayOfWeek((byte) 7, "Sunday");

        DayOfWeek[] daysOfWeek = new DayOfWeek[]{monday, tuesday, wednesday, thursday, friday, saturday, sunday};

        for (DayOfWeek day : daysOfWeek) {
            System.out.println(day.toString());
        }
    }
}
