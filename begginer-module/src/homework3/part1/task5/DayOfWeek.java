package homework3.part1.task5;

public class DayOfWeek {

    private byte dayNumber;
    private String dayName;

    private DayOfWeek() {
        super();
    }

    public DayOfWeek(byte dayNumber, String dayName) {
        super();
        this.dayNumber = dayNumber;
        this.dayName = dayName;
    }

    public byte getDayNumber() {
        return dayNumber;
    }

    public void setDayNumber(byte dayNumber) {
        this.dayNumber = dayNumber;
    }

    public String getDayName() {
        return dayName;
    }

    public void setDayName(String dayName) {
        this.dayName = dayName;
    }

    @Override
    public String toString() {
        return this.dayNumber + " " + this.dayName;
    }
}
