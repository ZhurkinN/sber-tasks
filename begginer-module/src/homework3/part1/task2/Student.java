package homework3.part1.task2;

import java.util.Arrays;

public class Student implements Comparable<Student> {

    private String name;
    private String surName;
    private int[] grades;

    public Student(String name, String surName, int[] grades) {
        super();
        this.name = name;
        this.surName = surName;
        this.grades = grades;
    }

    public void addGrade(int grade) {

        for (int i = 0; i < this.grades.length - 1; i++) {
            this.grades[i] = this.grades[i + 1];
        }
        this.grades[this.grades.length - 1] = grade;
    }

    public double getAverageGrade() {
        return Arrays.stream(this.grades).sum() / (1.0 * this.grades.length);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurName() {
        return surName;
    }

    public void setSurName(String surName) {
        this.surName = surName;
    }

    public int[] getGrades() {
        return grades;
    }

    public void setGrades(int[] grades) {
        this.grades = grades;
    }

    @Override
    public int compareTo(Student student) {
        return this.surName.compareTo(student.getSurName());
    }
}
