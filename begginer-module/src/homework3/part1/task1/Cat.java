package homework3.part1.task1;

public class Cat {

    private Cat() {
        super();
    }

    private static void sleep() {
        System.out.println("Sleep");
    }

    private static void meow() {
        System.out.println("Meow");
    }

    private static void eat() {
        System.out.println("Eat");
    }

    public static void status() {
        double random = Math.random();
        if (random < 1 / 3.0) {
            sleep();
        } else if (random < 2 / 3.0) {
            meow();
        } else {
            eat();
        }
    }
}
