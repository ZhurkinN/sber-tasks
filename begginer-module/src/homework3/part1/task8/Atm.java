package homework3.part1.task8;

public class Atm {

    private double rubbleToDollarCourse;
    private double dollarToRubbleCourse;
    private static int objectCounter;

    private Atm() {
        super();
    }

    public Atm(double rubbleToDollarCourse, double dollarToRubbleCourse) {
        super();
        if (rubbleToDollarCourse > 0) {
            this.rubbleToDollarCourse = rubbleToDollarCourse;
        }
        if (dollarToRubbleCourse > 0) {
            this.dollarToRubbleCourse = dollarToRubbleCourse;
        }
        Atm.objectCounter++;
    }

    public double convertDollarsToRubbles(double dollars) {
        return dollars * this.dollarToRubbleCourse;
    }

    public double convertRubblesToDollars(double rubbles) {
        return rubbles * this.rubbleToDollarCourse;
    }

    public static int getObjectCounter() {
        return Atm.objectCounter;
    }
}
