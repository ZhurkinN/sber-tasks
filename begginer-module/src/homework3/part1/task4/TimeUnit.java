package homework3.part1.task4;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

public class TimeUnit {

    private final String TWENTY_FOUR_HOUR_FORMAT = "HH:mm:ss";
    private final String TWELVE_HOUR_FORMAT = "hh:mm:ss a";

    private int hours;
    private int minutes;
    private int seconds;

    private TimeUnit() {
        super();
    }

    public TimeUnit(int hours, int minutes, int seconds) {
        super();
        setHoursAccordingToValidness(hours);
        setMinutesAccordingToValidness(minutes);
        setSecondsAccordingToValidness(seconds);
    }

    public TimeUnit(int hours, int minutes) {
        super();
        setHoursAccordingToValidness(hours);
        setMinutesAccordingToValidness(minutes);
        this.seconds = 0;
    }

    public TimeUnit(int hours) {
        super();
        setHoursAccordingToValidness(hours);
        this.minutes = 0;
        this.seconds = 0;
    }

    public void printTimeInTwentyFourHoursFormat() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(TWENTY_FOUR_HOUR_FORMAT);
        LocalTime time = LocalTime.of(this.hours, this.minutes, this.seconds);
        System.out.println(formatter.format(time));
    }

    public void printTimeInTwelveHoursFormat() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(TWELVE_HOUR_FORMAT);
        LocalTime time = LocalTime.of(this.hours, this.minutes, this.seconds);
        System.out.println(formatter.format(time));
    }

    public void addTime(int hours, int minutes, int seconds) {
        setHoursAccordingToValidness(this.hours + hours);
        setMinutesAccordingToValidness(this.minutes + minutes);
        setSecondsAccordingToValidness(this.seconds + seconds);
    }

    private void setHoursAccordingToValidness(int hours) {
        if (isHoursValid(hours)) {
            this.hours = hours;
        } else {
            this.hours = 0;
        }
    }

    private void setMinutesAccordingToValidness(int minutes) {
        if (isMinutesValid(minutes)) {
            this.minutes = minutes;
        } else {
            this.minutes = 0;
        }
    }

    private void setSecondsAccordingToValidness(int seconds) {
        if (isSecondsValid(seconds)) {
            this.seconds = seconds;
        } else {
            this.seconds = 0;
        }
    }

    private boolean isHoursValid(int hours) {
        return hours >= 0 && hours <= 23;
    }

    private boolean isMinutesValid(int minutes) {
        return minutes >= 0 && minutes <= 59;
    }

    private boolean isSecondsValid(int seconds) {
        return seconds >= 0 && seconds <= 59;
    }

    public int getHours() {
        return hours;
    }

    public void setHours(int hours) {
        this.hours = hours;
    }

    public int getMinutes() {
        return minutes;
    }

    public void setMinutes(int minutes) {
        this.minutes = minutes;
    }

    public int getSeconds() {
        return seconds;
    }

    public void setSeconds(int seconds) {
        this.seconds = seconds;
    }
}

