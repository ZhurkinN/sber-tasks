package homework1.part3;

import java.util.Scanner;

public class Task5 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int m = scanner.nextInt();
        int n = scanner.nextInt();
        int temp = n;
        if (m < n) {
            System.out.println(m);
        } else {
            while (m - temp >= n) {
                temp += n;
            }
            System.out.println(m - temp);
        }
        scanner.close();
    }
}
