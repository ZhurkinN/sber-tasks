package homework1.part3;

import java.util.Scanner;

public class Task7 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String string = scanner.nextLine();
        int length = 0;
        for (Character character : string.toCharArray()) {
            if (!character.equals(' ')) {
                length++;
            }
        }
        System.out.println(length);
        scanner.close();
    }
}
