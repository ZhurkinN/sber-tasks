package homework1.part3;

import java.util.Scanner;

public class Task10 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int row = 1;
        int whitespaces = (2 * n - 2) / 2;
        int tmp = whitespaces;
        String part = "#";
        while (row <= n) {
            String temp = "";
            for (int i = whitespaces; i > 0; i--) {
                temp += " ";
            }
            System.out.println(temp + part);
            part += "##";
            whitespaces--;
            row++;
        }
        String temp = "";
        for (int i = tmp; i > 0; i--) {
            temp += " ";
        }
        System.out.println(temp + "|");
        scanner.close();
    }
}
