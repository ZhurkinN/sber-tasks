package homework1.part3;

import java.util.Scanner;

public class Task6 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int eightBill = 0;
        int fourBill = 0;
        int twoBill = 0;
        int oneBill = 0;
        while (n >= 8) {
            n -= 8;
            eightBill++;
        }
        while (n >= 4) {
            n -= 4;
            fourBill++;
        }
        while (n >= 2) {
            n -= 2;
            twoBill++;
        }
        while (n >= 1) {
            n -= 1;
            oneBill++;
        }
        System.out.println(eightBill + " "
            + fourBill + " " + twoBill + " " + oneBill);
        scanner.close();
    }
}
