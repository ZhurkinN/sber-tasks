package homework1.part3;

import java.util.Scanner;

public class Task8 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int p = scanner.nextInt();
        int sum = 0;
        int a;
        for (int i = 1; i <= n; i++) {
            a = scanner.nextInt();
            if (a > p) {
                sum += a;
            }
        }
        System.out.println(sum);
        scanner.close();
    }
}
