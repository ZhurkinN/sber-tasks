package homework1.part3;

import java.util.Scanner;

public class Task4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Integer n = scanner.nextInt();
        for (Character digit : n.toString().toCharArray()) {
            System.out.println(digit);
        }
        scanner.close();
    }
}
