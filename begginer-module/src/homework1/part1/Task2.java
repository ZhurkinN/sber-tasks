package homework1.part1;

import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        int b = scanner.nextInt();
        double answer = Math.sqrt((Math.pow(a, 2) + Math.pow(b, 2)) / 2.0);
        System.out.println(answer);
        scanner.close();
    }
}
