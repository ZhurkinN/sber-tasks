package homework1.part1;

import java.util.Scanner;

public class Task6 {
    public static void main(String[] args) {
        final double KILOMETERS_PER_MILE = 1.60934d;
        Scanner scanner = new Scanner(System.in);
        double count = scanner.nextDouble();
        System.out.println(count / KILOMETERS_PER_MILE);
        scanner.close();
    }
}
