package homework1.part1;

import java.util.Scanner;

public class Task8 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double count = scanner.nextDouble();
        System.out.println(count / 30.0);
        scanner.close();
    }
}
