package homework1.part1;

import java.util.Scanner;

public class Task5 {

    public static void main(String[] args) {
        final double CENTIMETERS_PER_DIME = 2.54d;
        Scanner scanner = new Scanner(System.in);
        double dimes = scanner.nextDouble();
        System.out.println(dimes * CENTIMETERS_PER_DIME);
        scanner.close();
    }
}
