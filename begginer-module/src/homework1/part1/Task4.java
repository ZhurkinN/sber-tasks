package homework1.part1;

import java.util.Scanner;

public class Task4 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int count = scanner.nextInt();
        int hours = count / 3600;
        int minutes = count / 60 % 60;
        System.out.println(hours + " " + minutes);
        scanner.close();
    }
}
