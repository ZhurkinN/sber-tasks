package homework1.part2.extra;

import java.util.Scanner;

public class Task3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String model = scanner.nextLine();
        String answer;
        int price = Integer.parseInt(scanner.nextLine());
        if (model.contains("iphone")
                | model.contains("samsung")
                && price > 50000
                && price <= 120000) {
            answer = "Можно купить";
        } else {
            answer = "Не подходит";
        }
        System.out.println(answer);
        scanner.close();
    }
}
