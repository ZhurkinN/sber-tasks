package homework1.part2.extra;

import java.util.Scanner;

public class Task1 {
    public static void main(String[] args) {
        final int PASSWORD_LENGTH_BORDER = 8;
        Scanner scanner = new Scanner(System.in);
        String answer;
        String password = scanner.nextLine();
        boolean hasDigit = false;
        boolean hasSpecialSymbols = false;
        for (Character letter : password.toCharArray()) {
            if (Character.isDigit(letter)) {
                hasDigit = true;
            }
            if (letter.equals('_') || letter.equals('*') || letter.equals('-')) {
                hasSpecialSymbols = true;
            }
        }
        if (password.length() >= PASSWORD_LENGTH_BORDER
                && !password.equals(password.toLowerCase())
                && !password.equals(password.toUpperCase())
                && hasDigit
                && hasSpecialSymbols) {
            answer = "пароль надежный";
        } else {
            answer = "пароль не прошел проверку";
        }
        System.out.println(answer);
        scanner.close();
    }
}
