package homework1.part2.extra;

import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String answer;
        String mailPackage = scanner.nextLine();
        if (mailPackage.startsWith("камни!")
                | mailPackage.endsWith("камни!")
                && !mailPackage.contains("запрещенная продукция")) {
            answer = "камни в посылке";
        } else if (mailPackage.startsWith("запрещенная продукция")
                | mailPackage.endsWith("запрещенная продукция")
                && !mailPackage.contains("камни!")) {
            answer = "в посылке запрещенная продукция";
        } else if ((mailPackage.startsWith("камни!") && mailPackage.endsWith("запрещенная продукция"))
                || (mailPackage.startsWith("запрещенная продукция") && mailPackage.endsWith("камни!"))) {
            answer = "в посылке камни и запрещенная продукция";
        } else {
            answer = "все ок";
        }
        System.out.println(answer);
        scanner.close();
    }
}
