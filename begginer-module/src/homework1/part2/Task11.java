package homework1.part2;

import java.util.Scanner;

public class Task11 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int firstSide = scanner.nextInt();
        int secondSide = scanner.nextInt();
        int thirdSide = scanner.nextInt();
        int max = Math.max(Math.max(firstSide, secondSide), thirdSide);
        System.out.println(max < firstSide + secondSide + thirdSide - max);
        scanner.close();
    }
}
