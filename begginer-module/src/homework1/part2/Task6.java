package homework1.part2;

import java.util.Scanner;

public class Task6 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int count = scanner.nextInt();
        String level;
        if (count < 500) {
            level = "beginner";
        } else if (count < 1500) {
            level = "pre-intermediate";
        } else if (count < 2500) {
            level = "intermediate";
        } else if (count < 3500) {
            level = "upper-intermediate";
        } else {
            level = "fluent";
        }
        System.out.println(level);
        scanner.close();
    }
}
