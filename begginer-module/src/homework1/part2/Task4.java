package homework1.part2;

import java.util.Scanner;

public class Task4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int day= scanner.nextInt();

        System.out.println(day < 6 ? 6 - day : "Ура, выходные!");
        scanner.close();
    }
}
