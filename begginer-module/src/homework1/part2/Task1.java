package homework1.part2;

import java.util.Scanner;

public class Task1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int firstMark = scanner.nextInt();
        int secondMark = scanner.nextInt();
        int thirdMark = scanner.nextInt();

        String result;
        if (firstMark > secondMark && secondMark > thirdMark) {
            result = "Петя, пора трудиться";
        } else {
            result = "Петя молодец!";
        }
        System.out.println(result);
        scanner.close();
    }
}