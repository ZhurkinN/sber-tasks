package homework1.part2;

import java.util.Scanner;

public class Task3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int time = scanner.nextInt();

        System.out.println(time > 12 ? "Пора" : "Рано");
        scanner.close();
    }
}
