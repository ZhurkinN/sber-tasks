package homework1.part2;

import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) {
        final int BORDER_COORDINATE = 0;
        Scanner scanner = new Scanner(System.in);
        int firstCoordinate = scanner.nextInt();;
        int secondCoordinate = scanner.nextInt();

        System.out.println(firstCoordinate > BORDER_COORDINATE && secondCoordinate > BORDER_COORDINATE);
        scanner.close();
    }
}
