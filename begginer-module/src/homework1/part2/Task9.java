package homework1.part2;

import java.util.Scanner;

public class Task9 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double count = Math.toRadians(scanner.nextInt());
        System.out.println(Math.pow(Math.sin(count), 2) + Math.pow(Math.cos(count), 2) - 1 == 0);
        scanner.close();
    }
}
