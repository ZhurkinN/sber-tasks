package homework1.part2;

import java.util.Scanner;

public class Task10 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double count = scanner.nextDouble();
        System.out.println(Math.log(Math.pow(Math.E, count)) == count);
        scanner.close();
    }
}
