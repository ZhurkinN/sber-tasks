package homework1.part2;

import java.util.Scanner;

public class Task8 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String inputString = scanner.nextLine();
        int firstWhiteSpace = inputString.lastIndexOf(' ');
        String firstString = inputString.substring(0, firstWhiteSpace + 1);
        String secondString = inputString.substring(firstWhiteSpace + 1);
        System.out.println(firstString.trim() + "\n" + secondString);
        scanner.close();
    }
}
