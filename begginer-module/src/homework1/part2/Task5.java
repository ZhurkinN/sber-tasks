package homework1.part2;

import java.util.Scanner;

public class Task5 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        int b = scanner.nextInt();
        int c = scanner.nextInt();

        System.out.println((b * b - 4 * a * c) < 0 ? "Решения нет" : "Решение есть");
        scanner.close();
    }
}
