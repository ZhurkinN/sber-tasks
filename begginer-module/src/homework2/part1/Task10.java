package homework2.part1;

import java.util.Scanner;

public class Task10 {
    public static void main(String[] args) {
        run();
    }

    private static void run() {
        Scanner scanner = new Scanner(System.in);
        int hiddenNumber = (int) (Math.random() * 1000);
        while (true) {
            System.out.println("Попробуйте угадать случайное число от 0 до 1000");
            int guessedNumber = scanner.nextInt();
            if (guessedNumber < 0) {
                break;
            }
            if (guessedNumber == hiddenNumber) {
                System.out.println("Победа!");
                break;
            } else if (guessedNumber > hiddenNumber) {
                System.out.println("Это число больше загаданного");
            } else {
                System.out.println("Это число меньше загаданного");
            }
        }
        scanner.close();
    }
}
