package homework2.part1;

import java.util.Scanner;

public class Task9 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int arrayLength = scanner.nextInt();
        String[] strings = new String[arrayLength];
        String repeatedWord = "";
        scanner.nextLine();
        for (int i = 0; i < arrayLength; i++) {
            strings[i] = scanner.nextLine();
        }
        for (int i = 0; i < arrayLength; i++) {
            String currentWord = strings[i];
            for (int j = i + 1; j < arrayLength; j++) {
                if (currentWord.equals(strings[j])) {
                    repeatedWord += currentWord;
                    break;
                }
            }
        }
        System.out.println(repeatedWord);
        scanner.close();
    }
}
