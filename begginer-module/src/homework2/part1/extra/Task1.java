package homework2.part1.extra;

import java.util.Arrays;
import java.util.Scanner;

public class Task1 {
    public static void main(String[] args) {
        generatePassword();
    }

    private static void generatePassword() {
        final String UPPER_CHARACTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        final String LOWER_CHARACTERS = "abcdefghijklmnopqrstuvwxyz";
        final String DIGITS = "0123456789";
        final String SPECIAL_SYMBOLS = "_*-";
        final String ALL_SYMBOLS = UPPER_CHARACTERS + LOWER_CHARACTERS + DIGITS + SPECIAL_SYMBOLS;
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите длину желанного пароля:");
        int passwordLength;
        String password = "";
        while (true) {
            passwordLength = scanner.nextInt();
            if (passwordLength < 8) {
                System.out.println("Пароль с " + passwordLength + " количеством символов небезопасен");
            } else {
                break;
            }
        }
        char[] passwordLetters = new char[passwordLength];
        passwordLetters[0] = UPPER_CHARACTERS.charAt((int) (Math.random() * UPPER_CHARACTERS.length()));
        passwordLetters[1] = LOWER_CHARACTERS.charAt((int) (Math.random() * LOWER_CHARACTERS.length()));
        passwordLetters[passwordLength - 1] = SPECIAL_SYMBOLS.charAt((int) (Math.random() * SPECIAL_SYMBOLS.length()));
        passwordLetters[passwordLength - 2] = DIGITS.charAt((int) (Math.random() * DIGITS.length()));

        for (int i = 2; i < passwordLength - 2; i++) {
            passwordLetters[i] = ALL_SYMBOLS.charAt((int) (Math.random() * ALL_SYMBOLS.length()));
        }
        for (char letter : passwordLetters) {
            password += letter;
        }
        System.out.println("Ваш пароль - " + password);
        scanner.close();
    }
}
