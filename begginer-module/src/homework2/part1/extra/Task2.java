package homework2.part1.extra;

import java.util.Arrays;
import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int arrayLength = scanner.nextInt();
        int[] array = new int[arrayLength];
        for (int i = 0; i < arrayLength; i++) {
            array[i] = scanner.nextInt();
        }

        int[] newArray = new int[arrayLength];
        for (int i = 0; i < arrayLength; i++) {
            newArray[i] = (int) Math.pow(array[i], 2);
        }

        sortArrayByCountingMethod(newArray);
        System.out.print(newArray[0]);
        for (int i = 1; i < arrayLength; i++) {
            System.out.print(" " + newArray[i]);
        }
    }

    private static void sortArrayByCountingMethod(int[] array) {

        //Найдём максимальное число в массиве
        int k = array[0]; //Максимальное число в массиве
        for (int i = 1; i < array.length; i++) {
            if (array[i] > k) {
                k = array[i];
            }
        }

        //Создадим новый массив длины k, по умолчанию наполненный нулями
        int[] tempArray = new int[k + 1];

        //Запишем в него количество вхождений каждого элемента поиндексно
        for (int value : array) {
            ++tempArray[value];
        }

        //Вставим элементы в исходный массив
        int b = 0;
        for (int i = 0; i < k + 1; ++i){
            for (int j = 0; j < tempArray[i]; ++j) {
                array[b++] = i;
            }
        }
    }
}
