package homework2.part1;

import java.util.Scanner;

public class Task5 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int arrayLength = scanner.nextInt();
        int[] array = new int[arrayLength];
        int[] newArray = new int[arrayLength];
        for (int i = 0; i < arrayLength; i++) {
            array[i] = scanner.nextInt();
        }
        int shift = scanner.nextInt();
        for (int i = 0; i < shift; i++) {
            newArray[i] = array[arrayLength - shift + i];
        }
        for (int i = shift; i < arrayLength; i++) {
            newArray[i] = array[i-shift];
        }
        System.out.print(newArray[0]);
        for (int i = 1; i < arrayLength; i++) {
            System.out.print(" " + newArray[i]);
        }
        scanner.close();
    }
}
