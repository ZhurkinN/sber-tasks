package homework2.part1;

import java.util.Scanner;

public class Task8 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int arrayLength = scanner.nextInt();
        int[] array = new int[arrayLength];
        for (int i = 0; i < arrayLength; i++) {
            array[i] = scanner.nextInt();
        }
        int m = scanner.nextInt();
        int difference = Math.abs(array[0] - m);
        int value = array[0];
        for (int i = 1; i < arrayLength; i++) {
            int localDifference = Math.abs(array[i] - m);
            if (localDifference < difference) {
                difference = localDifference;
                value = array[i];
            } else if (localDifference == difference) {
                value = array[i];
            }
        }
        System.out.println(value);
        scanner.close();
    }
}
