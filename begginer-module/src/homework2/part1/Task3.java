package homework2.part1;

import java.util.Scanner;

public class Task3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int arrayLength = scanner.nextInt();
        int[] array = new int[arrayLength];
        for (int i = 0; i < arrayLength; i++) {
            array[i] = scanner.nextInt();
        }
        int element = scanner.nextInt();
        int elementIndex = 0;
        for (int i = 0; i < arrayLength; i++) {
            if (!(element >= array[i])) {
                elementIndex = i;
                break;
            }
        }
        System.out.println(elementIndex);
        scanner.close();
    }
}
