package homework2.part1;

import java.util.Scanner;

public class Task1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int arrayLength = scanner.nextInt();
        double[] array = new double[arrayLength];
        double sum = 0;
        for (int i = 0; i < arrayLength; i++) {
            array[i] = scanner.nextDouble();
            sum += array[i];
        }
        System.out.println(sum / arrayLength);
        scanner.close();
    }
}
