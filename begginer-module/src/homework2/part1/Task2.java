package homework2.part1;

import java.util.Arrays;
import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int firstArrayLength = scanner.nextInt();
        int[] firstArray = new int[firstArrayLength];
        for (int i = 0; i < firstArrayLength; i++) {
            firstArray[i] = scanner.nextInt();
        }
        int secondArrayLength = scanner.nextInt();
        int[] secondArray = new int[secondArrayLength];
        for (int i = 0; i < secondArrayLength; i++) {
            secondArray[i] = scanner.nextInt();
        }
        System.out.println(Arrays.equals(firstArray, secondArray));
        scanner.close();
    }
}
