package homework2.part1;

import java.util.Scanner;

public class Task4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int arrayLength = scanner.nextInt();
        int[] array = new int[arrayLength];
        for (int i = 0; i < arrayLength; i++) {
            array[i] = scanner.nextInt();
        }
        int element = array[0];
        int elementNumber = 0;
        for (int i = 0; i < arrayLength; i++) {
            if (array[i] != element) {
                System.out.println(elementNumber + " " + element);
                element = array[i];
                elementNumber = 1;
            } else {
                elementNumber++;
            }
        }
        System.out.println(elementNumber + " " + element);
        scanner.close();
    }
}
