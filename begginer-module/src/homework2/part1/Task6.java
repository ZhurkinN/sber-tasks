package homework2.part1;

import java.util.Scanner;

public class Task6 {
    public static void main(String[] args) {
        final int FIRST_ALPHABETIC_CODE = 1039;
        Scanner scanner = new Scanner(System.in);
        String inputWord = scanner.nextLine();
        String[] morseCodes = {".-", "-...", ".--", "--.", "-..", ".", "...-", "--..", "..", ".---", "-.-", ".-..", "--", "-.", "---", ".--.", ".-.",
                "...", "-", "..-", "..-.", "....", "-.-.", "---.", "----", "--.-", "--.--", "-.--", "-..-", "..-..", "..--", ".-.-"};
        String morseWord = "";
        for (Character letter : inputWord.toCharArray()) {
            int numberInAlphabet = (int) letter - FIRST_ALPHABETIC_CODE;
            morseWord += morseCodes[numberInAlphabet - 1] + " ";
        }
        System.out.println(morseWord);
        scanner.close();
    }
}
