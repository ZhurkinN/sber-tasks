package homework2.part1;

import java.util.Arrays;
import java.util.Scanner;

public class Task7 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int arrayLength = scanner.nextInt();
        int[] array = new int[arrayLength];
        for (int i = 0; i < arrayLength; i++) {
            array[i] = scanner.nextInt();
        }
        int[] newArray = new int[arrayLength];
        for (int i = 0; i < arrayLength; i++) {
            newArray[i] = (int) Math.pow(array[i], 2);
        }
        Arrays.sort(newArray);
        System.out.print(newArray[0]);
        for (int i = 1; i < arrayLength; i++) {
            System.out.print(" " + newArray[i]);
        }
    }
}
