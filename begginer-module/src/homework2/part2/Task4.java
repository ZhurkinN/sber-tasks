package homework2.part2;

import java.util.Arrays;
import java.util.Scanner;

public class Task4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int matrixLength = scanner.nextInt();
        int[][] matrix = new int[matrixLength][matrixLength];
        for (int row = 0; row < matrix.length; row++) {
            for (int column = 0; column < matrix[row].length; column++) {
                matrix[row][column] = scanner.nextInt();
            }
        }
        int deletedElementValue = scanner.nextInt();
        int[][] newMatrix = new int[matrixLength - 1][matrixLength - 1];

        int colCoordinate = 0;
        int rowCoordinate = 0;
        for(int i = 0; i < matrixLength; i++) {
            colCoordinate = Arrays.binarySearch(matrix[i], deletedElementValue);
            if (colCoordinate > 0) {
                rowCoordinate = i;
                break;
            }
        }

        for (int row = 0; row < rowCoordinate; row++) {
            for (int column = 0; column < colCoordinate; column++) {
                newMatrix[row][column] = matrix[row][column];
            }

            for (int column = colCoordinate + 1; column < matrixLength; column++) {
                newMatrix[row][column - 1] = matrix[row][column];
            }
        }


        for (int row = rowCoordinate + 1; row < matrixLength; row++) {
            for (int column = colCoordinate + 1; column < matrixLength; column++) {
                newMatrix[row - 1][column - 1] = matrix[row][column];
            }

            for (int column = 0; column < rowCoordinate; column++) {
                newMatrix[row - 1][column] = matrix[row][column];
            }
        }

        for (int row = 0; row < newMatrix.length; row++) {
            for (int column = 0; column < newMatrix[row].length - 1; column++) {
                System.out.print(newMatrix[row][column] + " ");
            }
            System.out.print(newMatrix[row][newMatrix[row].length - 1]);
            System.out.println();
        }
        scanner.close();
    }
}
