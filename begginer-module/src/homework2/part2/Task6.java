package homework2.part2;

import java.util.Arrays;
import java.util.Scanner;

public class Task6 {
    public static void main(String[] args) {
        final int DAYS_NUMBER = 7;
        final int NUTRIENTS_NUMBER = 4;

        Scanner scanner = new Scanner(System.in);
        int[] nutrients = new int[NUTRIENTS_NUMBER];
        int[][] dietArray = new int[DAYS_NUMBER][NUTRIENTS_NUMBER];
        boolean overeated = true;

        fillOneDimArray(nutrients, scanner);
        fillTwoDimArray(dietArray, scanner);

        int[] usedNutrients = new int[NUTRIENTS_NUMBER];
        Arrays.fill(usedNutrients, 0);
        for (int row = 0; row < dietArray.length; row++) {
            for (int column = 0; column < dietArray[row].length; column++) {
                usedNutrients[column] += dietArray[row][column];
            }
        }

        for (int i = 0; i < NUTRIENTS_NUMBER; i++) {
            if (usedNutrients[i] > nutrients[i]) {
                overeated = false;
                break;
            }
        }

        System.out.println(overeated ? "Отлично" : "Нужно есть поменьше");
    }

    private static void fillOneDimArray(int[] array, Scanner scanner) {
        for (int i = 0; i < array.length; i++) {
            array[i] = scanner.nextInt();
        }
    }

    private static void fillTwoDimArray(int[][] array, Scanner scanner) {
        for (int row = 0; row < array.length; row++) {
            for (int column = 0; column < array[row].length; column++) {
                array[row][column] = scanner.nextInt();
            }
        }
    }
}
