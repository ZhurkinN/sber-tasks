package homework2.part2;

import java.util.Scanner;

public class Task1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int columns = scanner.nextInt();
        int rows = scanner.nextInt();
        int[][] matrix = new int[rows][columns];
        int[] minimumsInRows = new int[rows];
        for (int row = 0; row < matrix.length; row++) {
            for (int column = 0; column < matrix[row].length; column++) {
                matrix[row][column] = scanner.nextInt();
            }
        }

        for (int row = 0; row < matrix.length; row++) {
            int minimum = matrix[row][0];
            for (int column = 1; column < matrix[row].length; column++) {
                if (matrix[row][column] < minimum) {
                    minimum = matrix[row][column];
                }
            }
            minimumsInRows[row] = minimum;
        }

        System.out.print(minimumsInRows[0]);
        for (int i = 1; i < minimumsInRows.length; i++) {
            System.out.print(" " + minimumsInRows[i]);
        }
    }
}
