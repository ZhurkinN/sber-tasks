package homework2.part2;

import java.util.Scanner;

public class Task9 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int number = scanner.nextInt();
        String output = "";

        String outString = new StringBuilder(getStringOfDigits(number, output))
                .reverse()
                .toString();

        System.out.println(outString);
        scanner.close();
    }

    private static String getStringOfDigits(int number, String output) {
        if (number / 10 < 1) {
            return output + number;
        } else {
            return output + number % 10 + " " + getStringOfDigits(number / 10, output);
        }
    }
}
