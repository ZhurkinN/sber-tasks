package homework2.part2;

import java.util.Arrays;
import java.util.Scanner;

public class Task7 {
    public static void main(String[] args) {
        final int JUDGE_NUMBER = 3;
        final int ROWS_NUMBER = 3;
        Scanner scanner = new Scanner(System.in);
        int participantNumber = scanner.nextInt();
        String[] owners = new String[participantNumber];
        String[] dogs = new String[participantNumber];
        int[][] marks = new int[participantNumber][JUDGE_NUMBER];
        float[] avgMarks = new float[participantNumber];
        int[] indexes = new int[ROWS_NUMBER];

        scanner.nextLine();
        fillArray(owners, scanner);
        fillArray(dogs, scanner);
        fillTwoDimArray(marks, scanner);
        fillAverageMarks(marks, avgMarks, JUDGE_NUMBER);
        fillMaxIndexes(Arrays.copyOf(avgMarks, avgMarks.length), indexes);

        printResults(owners, dogs, avgMarks, indexes);
        scanner.close();
    }

    private static void printResults(String[] owners, String[] dogs, float[] avgMarks, int[] indexes) {
        for (int index : indexes) {
            System.out.println(owners[index]
                    + ": "
                    + dogs[index]
                    + ", "
                    + Math.floor(avgMarks[index] * 10) / 10.0);
        }
    }

    private static void fillArray(String[] array, Scanner scanner) {
        for (int i = 0; i < array.length; i++) {
            array[i] = scanner.nextLine();
        }
    }

    private static void fillTwoDimArray(int[][] array, Scanner scanner) {
        for (int row = 0; row < array.length; row++) {
            for (int column = 0; column < array[row].length; column++) {
                array[row][column] = scanner.nextInt();
            }
        }
    }

    private static void fillAverageMarks(int[][] marks, float[] averageMarks, int judgeNumber) {
        for (int row = 0; row < marks.length; row++) {
            float rowSum = 0;
            rowSum += Arrays.stream(marks[row]).sum();
            averageMarks[row] = rowSum / judgeNumber;
        }
    }

    private static void fillMaxIndexes(float[] array, int[] indexes) {
        int i = 1;
        while (i != indexes.length + 1) {
            float max = array[0];
            int index = 0;
            for (int j = 1; j < array.length; j++) {
                if (array[j] > max) {
                    max = array[j];
                    index = j;
                }
            }
            array[index] = 0;
            indexes[i - 1] = index;
            i++;
        }
    }
}
