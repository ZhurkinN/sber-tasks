package homework2.part2;

import java.util.Scanner;

public class Task10 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int number = scanner.nextInt();
        String output = "";
        System.out.println(getStringOfDigits(number, output));
        scanner.close();
    }

    private static String getStringOfDigits(int number, String output) {
        if (number / 10 < 1) {
            return output + number;
        } else {
            return output + number % 10 + " " + getStringOfDigits(number / 10, output);
        }
    }
}
