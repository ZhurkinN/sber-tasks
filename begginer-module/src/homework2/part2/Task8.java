package homework2.part2;

import java.util.Scanner;

public class Task8 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int number = scanner.nextInt();
        System.out.println(countDigitSum(number));
        scanner.close();
    }

    private static int countDigitSum(int number) {
        if (number / 10 < 1) {
            return number % 10;
        } else {
            return number % 10 + countDigitSum(number / 10);
        }
    }
}
