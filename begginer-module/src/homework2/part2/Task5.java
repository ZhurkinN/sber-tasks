package homework2.part2;

import java.util.Scanner;

public class Task5 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int matrixLength = scanner.nextInt();
        int[][] matrix = new int[matrixLength][matrixLength];
        boolean isSymmetric = true;
        for (int row = 0; row < matrix.length; row++) {
            for (int column = 0; column < matrix[row].length; column++) {
                matrix[row][column] = scanner.nextInt();
            }
        }

        for (int row = 0; row < matrix.length; row++) {
            for (int column = 0; column < matrix[row].length; column++) {
                if (matrix[row][column] != matrix[matrixLength - 1 - column][matrixLength - 1 - row]) {
                    isSymmetric = false;
                    break;
                }
            }
        }

        System.out.println(isSymmetric);
        scanner.close();
    }
}
