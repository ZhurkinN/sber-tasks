package homework2.part2;

import java.util.Arrays;
import java.util.Scanner;

public class Task3 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int matrixLength = scanner.nextInt();
        int yCoordinate = scanner.nextInt();
        int xCoordinate = scanner.nextInt();
        String[][] matrix = new String[matrixLength][matrixLength];
        for(int i = 0; i < matrixLength; i++) {
            Arrays.fill(matrix[i], "0");
        }

        matrix[xCoordinate][yCoordinate] = "K";

        if (xCoordinate - 2 >= 0) {
            if (yCoordinate - 1 >= 0) {
                matrix[xCoordinate - 2][yCoordinate - 1] = "X";
            }
            if (yCoordinate + 1 <= matrixLength - 1) {
                matrix[xCoordinate - 2][yCoordinate + 1] = "X";
            }
        }

        if (xCoordinate - 1 >= 0) {
            if (yCoordinate - 2 >= 0) {
                matrix[xCoordinate - 1][yCoordinate - 2] = "X";
            }
            if (yCoordinate + 2 <= matrixLength - 1) {
                matrix[xCoordinate - 1][yCoordinate + 2] = "X";
            }
        }

        if (xCoordinate + 2 <= matrixLength - 1) {
            if (yCoordinate - 1 >= 0) {
                matrix[xCoordinate + 2][yCoordinate - 1] = "X";
            }
            if (yCoordinate + 1 <= matrixLength - 1) {
                matrix[xCoordinate + 2][yCoordinate + 1] = "X";
            }
        }

        if (xCoordinate + 1 <= matrixLength - 1) {
            if (yCoordinate - 2 >= 0) {
                matrix[xCoordinate + 1][yCoordinate - 2] = "X";
            }
            if (yCoordinate + 2 <= matrixLength - 1) {
                matrix[xCoordinate + 1][yCoordinate + 2] = "X";
            }
        }

        for (int row = 0; row < matrix.length; row++) {
            for (int column = 0; column < matrix[row].length - 1; column++) {
                System.out.print(matrix[row][column] + " ");
            }
            System.out.print(matrix[row][matrix[row].length - 1]);
            System.out.println();
        }
        scanner.close();
    }
}
