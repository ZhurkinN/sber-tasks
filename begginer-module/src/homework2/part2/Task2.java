package homework2.part2;

import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int matrixLength = scanner.nextInt();
        int y1 = scanner.nextInt();
        int x1 = scanner.nextInt();
        int y2 = scanner.nextInt();
        int x2 = scanner.nextInt();
        int[][] matrix = new int[matrixLength][matrixLength];

        for (int row = 0; row < matrix.length; row++) {
            for (int column = 0; column < matrix[row].length; column++) {
                if ((column == y1 && row >= x1 && row <= x2)
                        || (column == y2 && row >= x1 && row <= x2)
                        || (row == x1 && column >= y1 && column <= y2)
                        || (row == x2 && column >= y1 && column <= y2)) {
                    matrix[row][column] = 1;
                } else {
                    matrix[row][column] = 0;
                }
            }
        }

        for (int row = 0; row < matrix.length; row++) {
            for (int column = 0; column < matrix[row].length - 1; column++) {
                System.out.print(matrix[row][column] + " ");
            }
            System.out.print(matrix[row][matrix[row].length - 1]);
            System.out.println();
        }
        scanner.close();
    }
}
