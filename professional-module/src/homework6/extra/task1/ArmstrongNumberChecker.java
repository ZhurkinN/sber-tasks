package homework6.extra.task1;

public class ArmstrongNumberChecker {

    public static boolean isArmstrongNumber(int number) {

        if (number < 0) {
            return false;
        }

        String numberString = Integer.toString(number);
        int numberLength = numberString.length();
        char[] numberDigits = numberString.toCharArray();

        int newNumber = 0;
        for (int i = 0; i < numberLength; i++) {
            int digit = numberDigits[i] - '0';
            newNumber += Math.pow(digit, numberLength);
        }

        return number == newNumber;

    }
}
