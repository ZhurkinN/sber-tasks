package homework6.extra.task1;

public class Runner {

    public static void main(String[] args) {
        printTestResult(121);
        printTestResult(153);
        printTestResult(371);
    }

    private static void printTestResult(int number) {
        System.out.println(number +
                " - " +
                ArmstrongNumberChecker.isArmstrongNumber(number));
    }
}
