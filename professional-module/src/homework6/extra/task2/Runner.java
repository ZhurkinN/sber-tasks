package homework6.extra.task2;

public class Runner {

    public static void main(String[] args) {

        for (int i = 1; i < 100; i++) {
            if (PrimeNumberChecker.isPrimeNumber(i)) {
                printTestResult(i);
            }
        }
    }

    private static void printTestResult(int number) {
        System.out.println(number +
                " - " +
                PrimeNumberChecker.isPrimeNumber(number));
    }
}
