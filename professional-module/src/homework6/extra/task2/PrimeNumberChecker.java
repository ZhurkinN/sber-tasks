package homework6.extra.task2;

public class PrimeNumberChecker {

    public static boolean isPrimeNumber(int number) {

        if (number <= 1) {
            return false;
        }

        int halfOfNumber = number / 2;
        boolean isPrime = true;

        for (int i = 2; i <= halfOfNumber; i++) {
            if (number % i == 0) {
                isPrime = false;
                break;
            }
        }

        return isPrime;
    }
}
