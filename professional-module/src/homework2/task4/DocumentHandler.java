package homework2.task4;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class DocumentHandler {

    public static Map<Integer, Document> organizeDocuments(List<Document> documents) {
        HashMap<Integer, Document> organizedDocuments = new HashMap<>();
        Iterator<Document> iterator = documents.iterator();

        while (iterator.hasNext()) {
            Document document = iterator.next();
            organizedDocuments.put(document.getId(), document);
        }

        return organizedDocuments;
    }
}
