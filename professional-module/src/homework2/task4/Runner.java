package homework2.task4;

import java.util.ArrayList;
import java.util.List;

public class Runner {

    public static void main(String[] args) {

        Document document1 = new Document(1, "ddfdf", 434);
        Document document2 = new Document(2, "ddsddfw3", 234);
        List<Document> documents = new ArrayList<>();
        documents.add(document1);
        documents.add(document2);

        System.out.println(DocumentHandler.organizeDocuments(documents));
    }
}
