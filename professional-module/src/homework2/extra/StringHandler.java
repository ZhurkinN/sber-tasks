package homework2.extra;

import java.util.*;

public class StringHandler {

    public static String[] getMostUsedWords(String[] words, int k) {
        Map<String, Integer> wordsFreqInfo = new HashMap<>();
        for (String word : words) {
            if (wordsFreqInfo.containsKey(word)) {
                wordsFreqInfo.put(word, wordsFreqInfo.get(word) + 1);
            } else {
                wordsFreqInfo.put(word, 1);
            }
        }

        return sortByValue(wordsFreqInfo, k);
    }

    private static String[] sortByValue(Map<String, Integer> wordsFreqInfo, int k) {

        List<Map.Entry<String, Integer>> wordsFreqInfoList = new ArrayList<>(wordsFreqInfo.entrySet());
        wordsFreqInfoList.sort((o1, o2) ->  {
            if (o1.getValue().equals(o2.getValue())) {
                String s1 = o1.getKey();
                String s2 = o2.getKey();

                for (int i = 0; i < s1.length() && i < s2.length(); i++) {
                    if ((int) s1.charAt(i) != (int) s2.charAt(i)) {
                        return (int) s2.charAt(i) - (int) s1.charAt(i);
                    }
                }

                return - s1.compareTo(s2);

            } else {
                return o1.getValue().compareTo(o2.getValue());
            }
        });

        Collections.reverse(wordsFreqInfoList);
        wordsFreqInfoList = wordsFreqInfoList.subList(0, k);

        ArrayList<String> mostUsedWords = new ArrayList<>();
        wordsFreqInfoList.forEach(e -> mostUsedWords.add(e.getKey()));

        return mostUsedWords.toArray(new String[0]);
    }
}
