package homework2.extra;

import java.util.Arrays;

public class Runner {

    public static void main(String[] args) {
        String[] words = new String[] {"a","aa","aaa"};
        int k = 2;

        System.out.println(Arrays.toString(StringHandler.getMostUsedWords(words, k)));
    }
}
