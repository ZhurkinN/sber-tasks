package homework2.task2;

import java.util.List;
import java.util.Scanner;

public class Runner {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String s = scanner.nextLine();
        String t = scanner.nextLine();

        System.out.println(isAnagram(s, t));
        scanner.close();
    }

    public static boolean isAnagram(String s, String t) {
        if (s.length() != t.length()) {
            return false;
        }
        return getUniqueSymbols(s).equals(getUniqueSymbols(t));
    }

    private static List<Character> getUniqueSymbols(String s) {
        s = s.replaceAll("\\p{Punct}", "")
                .replaceAll(" ", "")
                .toLowerCase();
        return s.chars()
                .mapToObj(c -> (char) c)
                .sorted().toList();
    }
}
