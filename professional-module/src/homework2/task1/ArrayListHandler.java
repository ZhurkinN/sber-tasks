package homework2.task1;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class ArrayListHandler {

    public static <T> Set<T> getUniqueElements(ArrayList<T> elements) {
        return new HashSet<>(elements);
    }
}
