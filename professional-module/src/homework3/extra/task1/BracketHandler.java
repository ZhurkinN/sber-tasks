package homework3.extra.task1;

public class BracketHandler {

    public static boolean areBracketsValid(String sentence) {
        if (sentence.length() % 2 == 1) {
            return false;
        }
        if (sentence.length() == 0) {
            return true;
        }
        char[] brackets = sentence.toCharArray();
        int opened = 0;
        int closed = 0;
        boolean isValid = true;
        for (char bracket : brackets) {
            if (bracket == '(') {
                opened++;
            } else {
                closed++;
            }
            if (opened < closed) {
                isValid = false;
                break;
            }
        }

        return isValid;
    }

}
