package homework3.extra.task1;

public class Runner {

    public static void main(String[] args) {

        printTestResult("(()()())");
        printTestResult(")(");
        printTestResult("(()");
        printTestResult("(()))");
        printTestResult("");
    }

    private static void printTestResult(String sentence) {
        System.out.println("Sentence - " + sentence + "; result - " + BracketHandler.areBracketsValid(sentence));
    }
}
