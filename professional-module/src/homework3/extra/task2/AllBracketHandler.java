package homework3.extra.task2;

import java.util.Stack;

public class AllBracketHandler {

    public static boolean areBracketsValid(String sentence) {
        Stack<Character> stack = new Stack<>();
        char[] chars = sentence.toCharArray();

        for (int i = 0; i < sentence.length(); i++) {

            if (chars[i] == '(' || chars[i] == '{' || chars[i] == '[') {
                stack.push(chars[i]);
                continue;
            }

            if (!stack.isEmpty() && (chars[i] == ')' && stack.peek() == '('
                    || chars[i] == '}' && stack.peek() == '{'
                    || chars[i] == ']' && stack.peek() == '[')) {
                stack.pop();
            } else {
                return false;
            }
        }

        return stack.isEmpty();
    }

}
