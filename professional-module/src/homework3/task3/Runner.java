package homework3.task3;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Runner {

    public static void main(String[] args) {
        Class<APrinter> aPrinterClass = APrinter.class;

        try {
            Method method = APrinter.class.getMethod("print", int.class);
            Object object = aPrinterClass.getDeclaredConstructor().newInstance();
            method.invoke(object, 25);
        } catch (NoSuchMethodException e) {
            System.out.println("NoSuchMethodException thrown.");
        } catch (InstantiationException e) {
            System.out.println("InstantiationException thrown.");
        } catch (IllegalAccessException e) {
            System.out.println("IllegalAccessException thrown.");
        } catch (InvocationTargetException e) {
            System.out.println("InvocationTargetException thrown.");
        } catch (IllegalArgumentException e) {
            System.out.println("IllegalArgumentException thrown.");
        } catch (SecurityException e) {
            System.out.println("SecurityException thrown.");
        }
    }
}
