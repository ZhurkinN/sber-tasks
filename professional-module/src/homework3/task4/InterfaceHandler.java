package homework3.task4;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class InterfaceHandler {

    public static Set<Class<?>> findInterfaces(Class<?> clazz) {
        Set<Class<?>> interfaces = new HashSet<>();
        Class<?> classIteration = clazz;

        do {
            List<Class<?>> parentInterfaces = List.of(classIteration.getInterfaces());
            interfaces.addAll(parentInterfaces);

            for (Class<?> clasz : parentInterfaces) {
                interfaces.addAll(List.of(clasz.getInterfaces()));
            }
            classIteration = classIteration.getSuperclass();
        } while (classIteration != null);

        return interfaces;
    }
}
