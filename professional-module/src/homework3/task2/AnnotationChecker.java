package homework3.task2;

import homework3.task1.IsLike;

public class AnnotationChecker {

    public static void checkAnnotationUsed(Class<?> clazz) {
        String outString = "Для класса " + clazz.getName();
        if (clazz.isAnnotationPresent(IsLike.class)) {
            outString += " аннотация IsLike присутствует и её значение - " + clazz.getDeclaredAnnotation(IsLike.class).liked();
        } else {
            outString += " аннотация IsLike отсутствует";
        }
        System.out.println(outString);

    }

    public static void main(String[] args) {

        checkAnnotationUsed(Cat.class);
        checkAnnotationUsed(Dog.class);

    }
}
