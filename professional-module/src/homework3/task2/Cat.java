package homework3.task2;

import homework3.task1.IsLike;

@IsLike(liked = true)
public class Cat {

    private String name;

    public Cat(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
