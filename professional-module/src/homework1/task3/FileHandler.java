package homework1.task3;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

public class FileHandler {

    public static void writeFile() {

        List<String> strings = new ArrayList<>();

        try (Scanner scanner = new Scanner(new File("professional-module\\src\\homework1\\task3\\input.txt"))) {

            while (scanner.hasNext()) {
                strings.add(scanner.nextLine().toUpperCase(Locale.ENGLISH));
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        try (Writer writer = new FileWriter("professional-module\\src\\homework1\\task3\\output.txt")) {

            BufferedWriter bufferedWriter = new BufferedWriter(writer);
            for (String string : strings) {
                bufferedWriter.write(string);
                bufferedWriter.newLine();
            }
            bufferedWriter.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
