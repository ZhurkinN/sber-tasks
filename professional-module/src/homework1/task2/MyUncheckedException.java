package homework1.task2;

public class MyUncheckedException extends ArithmeticException {

    public MyUncheckedException(String message) {
        super(message);
    }

    public MyUncheckedException() {
        super("Divide by zero.");
    }
}
