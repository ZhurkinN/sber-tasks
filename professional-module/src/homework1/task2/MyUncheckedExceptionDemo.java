package homework1.task2;

public class MyUncheckedExceptionDemo {
    public static void main(String[] args) {
        try {
            testException();
        } catch (MyUncheckedException e) {
            e.printStackTrace();
        }
    }

    public static void testException() throws MyUncheckedException {
        throw new MyUncheckedException();
    }
}
