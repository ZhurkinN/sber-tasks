package homework1.task4;

public class MyEvenNumber {

    private int n;

    public MyEvenNumber(int number) throws OddNumberException {
        if (number % 2 == 1) {
            throw new OddNumberException();
        }
        this.n = number;
    }

    @Override
    public String toString() {
        return "Even number is " + this.n;
    }
}
