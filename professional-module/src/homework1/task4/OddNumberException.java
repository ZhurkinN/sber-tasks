package homework1.task4;

public class OddNumberException extends Exception {

    public OddNumberException() {
        super("Can't create object with odd number.");
    }
}
