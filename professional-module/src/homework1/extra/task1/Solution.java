package homework1.extra.task1;

import java.util.Scanner;

public class Solution {

    private static final int MAXIMUMS_ARRAY_LENGTH = 2;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int length = scanner.nextInt();
        int[] array = new int[length];
        int[] maximums;
        scanner.nextLine();

        for (int i = 0; i < length; i++) {
            array[i] = scanner.nextInt();
        }

        maximums = findCoupleOfMaximums(array);

        System.out.println(maximums[0] + " " + maximums[1]);
    }

    private static int[] findCoupleOfMaximums(int[] array) {
        int[] maximums = new int[MAXIMUMS_ARRAY_LENGTH];

        for (int i = 0; i < maximums.length; i++) {
            maximums[i] = findMaximum(array);
        }


        return maximums;
    }

    private static int findMaximum(int[] array) {
        int maximum = array[0];
        int maximumIndex = 0;

        for (int i = 1; i < array.length; i++) {
            if (array[i] > maximum) {
                maximum = array[i];
                maximumIndex = i;
            }
        }

        array[maximumIndex] = Integer.MIN_VALUE;
        return maximum;
    }
}
