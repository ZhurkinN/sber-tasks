package homework1.extra.task2;

import java.util.Scanner;

public class Solution {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int length = scanner.nextInt();
        int[] array = new int[length];
        scanner.nextLine();

        for (int i = 0; i < length; i++) {
            array[i] = scanner.nextInt();
        }
        int searchedElement = scanner.nextInt();

        System.out.println(binarySearch(array, searchedElement, 0, array.length - 1));


    }

    private static int binarySearch(int[] array, int searchedElement, int bottom, int top) {

        if (bottom == top) {
            if (array[bottom] == searchedElement) {
                return bottom;
            }
            return -1;
        }

        int middle = (top - bottom) / 2 + bottom;

        if (array[middle] > searchedElement) {
            if (middle != 0) {
                return binarySearch(array, searchedElement, bottom, middle - 1);
            } else {
                return -1;
            }
        } else if (array[middle] < searchedElement) {
            if (middle != array.length - 1) {
                return binarySearch(array, searchedElement, middle + 1, top);
            } else {
                return -1;
            }
        }
        return middle;

    }
}
