package homework1.task6;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

public class FormValidator {

    public static void checkName(String name) throws InvalidParameterException {

        if (name.length() <= 2 || name.length() >= 20) {
            throw new InvalidParameterException("Wrong length of name.");
        }

        if (Character.isLowerCase(name.charAt(0))) {
            throw new InvalidParameterException("First letter of name is lower.");
        }
    }

    public static void checkBirthdate(String birthdate) throws DateTimeParseException, InvalidParameterException {

        DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("yyyy.MM.dd");
        LocalDate date = LocalDate.parse(birthdate, dateFormatter);
        LocalDate firstDate = LocalDate.parse("1900.01.01", dateFormatter);
        LocalDate currentDate = LocalDate.now();

        if (date.isBefore(firstDate) || date.isAfter(currentDate)) {
            throw new InvalidParameterException("Birthday is out of bounds.");
        }
    }

    public static void checkGender(String gender) throws InvalidParameterException {

        if (!gender.equals(GenderEnum.MALE.getGender()) && !gender.equals(GenderEnum.FEMALE.getGender())) {
            throw new InvalidParameterException("Wrong gender.");
        }
    }

    public static void checkHeight(String height) throws InvalidParameterException {

        double heightNumber;
        try {
            heightNumber = Double.parseDouble(height);
        } catch (NumberFormatException e) {
            throw new InvalidParameterException("Can't cast height to double.");
        }

        if (heightNumber < 0) {
            throw new InvalidParameterException("Height is negative.");
        }

    }
}
