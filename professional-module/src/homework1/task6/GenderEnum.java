package homework1.task6;

public enum GenderEnum {

    MALE("Male"),
    FEMALE("Female");

    private String gender;

    GenderEnum(String gender) {
        this.gender = gender;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }
}
