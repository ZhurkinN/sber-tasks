package homework1.task6;

import java.time.format.DateTimeParseException;

public class FormValidatorDemo {

    public static void main(String[] args) {

        try {
            FormValidator.checkName("Alexey");
            FormValidator.checkBirthdate("2012.01.01");
            FormValidator.checkGender("Male");
            FormValidator.checkHeight("230");
        } catch (DateTimeParseException | InvalidParameterException e) {
            e.printStackTrace();
        }
    }
}
