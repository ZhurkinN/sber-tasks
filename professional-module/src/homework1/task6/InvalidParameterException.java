package homework1.task6;

public class InvalidParameterException extends Exception{

    public InvalidParameterException(String message) {
        super(message);
    }
}
