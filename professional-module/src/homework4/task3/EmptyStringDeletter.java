package homework4.task3;

import java.util.ArrayList;
import java.util.List;

public class EmptyStringDeletter {

    private static int findNumberOfNotEmptyStrings(List<String> strings) {
        return strings
                .stream()
                .filter(s -> !s.isEmpty())
                .toList()
                .size();
    }

    public static void main(String[] args) {
        List<String> strings = new ArrayList<>();
        strings.add("abc");
        strings.add("");
        strings.add("");
        strings.add("def");
        strings.add("ghi");
        System.out.println(strings
                + " - "
                + findNumberOfNotEmptyStrings(strings));
    }
}
