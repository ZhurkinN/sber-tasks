package homework4.extra;

import java.util.*;

public class StringHandler {

    public static boolean canCompareStrings(String firstString,
                                            String secondString) {
        if (firstString.length() - secondString.length() > 1
                || firstString.length() - secondString.length() < -1) {
            return false;
        }

        return canCompareStringsUsingAddingOrDeletingSymbol(firstString, secondString)
                || canCompareUsingReplacingSymbol(firstString, secondString);
    }

    private static boolean canCompareStringsUsingAddingOrDeletingSymbol(String firstString,
                                                                        String secondString) {

        if (firstString.length() - secondString.length() == 1) {
            return firstString.substring(0, firstString.length() - 1).equals(secondString);
        } else if (firstString.length() - secondString.length() == -1) {
            return secondString.substring(0, secondString.length() - 1).equals(firstString);
        } else {
            return false;
        }
    }

    private static boolean canCompareUsingReplacingSymbol(String firstString,
                                                          String secondString) {
        if (firstString.length() == secondString.length()) {
            char[] fistStringSymbols = firstString.toCharArray();
            char[] secondStringSymbols = secondString.toCharArray();
            int counter = 0;

            for (int i = 0; i < fistStringSymbols.length; i++) {
                if (fistStringSymbols[i] == secondStringSymbols[i]) {
                    counter++;
                }
            }

            return counter == firstString.length() - 1;
        } else {
            return false;
        }
    }



    public static void main(String[] args) {
        printTestResults("cat", "cats");
        printTestResults("cat", "tacs");
        printTestResults("cat", "cut");
        printTestResults("cat", "nut");
    }

    private static void printTestResults(String firstString,
                                         String secondString) {
        String stringBuilder = firstString +
                " и " +
                secondString +
                " -> " +
                canCompareStrings(firstString, secondString);
        System.out.println(stringBuilder);
    }
}
