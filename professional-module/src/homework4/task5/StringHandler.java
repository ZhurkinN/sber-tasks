package homework4.task5;

import java.util.List;
import java.util.stream.Collectors;

public class StringHandler {

    private static void printChangedToUpperCaseStrings(List<String> strings) {
        System.out.println(strings.stream()
                .map(String::toUpperCase)
                .collect(Collectors.joining(", ")) + ".");
    }

    public static void main(String[] args) {
        printChangedToUpperCaseStrings(List.of("abc", "def", "qqq"));
    }
}
