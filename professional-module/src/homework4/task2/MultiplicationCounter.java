package homework4.task2;

import java.util.List;

public class MultiplicationCounter {

    public static void main(String[] args) {
        System.out.println(findMultiplication(List.of(2, 4, 3, 5)));
        System.out.println(findMultiplication(List.of(2, 4, 3, 5, 6, 7, 8, 9, 10)));
    }

    private static int findMultiplication(List<Integer> list) {
        return list
                .stream()
                .mapToInt(e -> e)
                .reduce(1, (a, b) -> a*b);
    }
}
