package homework4.task4;

import java.util.Collections;
import java.util.List;

public class RealNumbersSorter {

    private static List<Double> sortRealNumbersList(List<Double> numbers) {
        return numbers
                .stream()
                .sorted(Collections.reverseOrder())
                .toList();
    }

    public static void main(String[] args) {
        List<Double> numbers = List.of(1.2, 4.2, 0.2, 0.1, 4.3, 6.4, 1.0);
        System.out.println(sortRealNumbersList(numbers));
    }
}
