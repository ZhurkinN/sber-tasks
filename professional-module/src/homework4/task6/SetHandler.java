package homework4.task6;

import java.util.Set;
import java.util.stream.Collectors;

public class SetHandler {

    private static Set<Integer> reduceSetLevel(Set<Set<Integer>> setOfSets) {
        return setOfSets.stream()
                .flatMap(Set::stream)
                .collect(Collectors.toSet());
    }
}
